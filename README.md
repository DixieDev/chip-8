#CHIP-8

CHIP-8 is a simple interpreted language designed for easily making games on 
old machines that typically had no more than 4kb of memory. 

This project is a simple interpreter for CHIP-8 that also handles emulation of
an old machine's memory. There are no plans to support audio, because it is a 
bit of a pain to setup in exchange for being able to produce a single tone beep.

##Project structure
There are two crates: 
 - The chip8 crate is a lib that handles all the CHIP-8 internals, exposing 
   access to a few functions that can be used to load data into memory, clock
   the 'processor', handle inputs, and get screen data.
 - The root crate is a binary that simply makes the CHIP-8 struct, loads a rom, 
   handles clocking the 'processor' at a fixed rate, sends inputs, and displays 
   the screen's pixels in a window.
