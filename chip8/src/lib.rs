mod font;
pub mod ops;

use ops::Op;
use rand::prelude::*;

pub const MAIN_ADDR: u16 = 0x200;  // First 512 bytes are reserved for interpreter
const STACK_ADDR: u16 = 0xEFF; // Stack extends downwards?
const F: usize = 0xF;

enum BlitResult {
    Unset,
    NotUnset,
}

struct Screen {
    pixels: Vec<u8>,
}

impl Screen {
    fn new() -> Self {
        Self { pixels: vec![0; 64*32] }
    }

    fn clear(&mut self) {
        for pixel in self.pixels.iter_mut() {
            *pixel = 0;
        }
    }

    fn blit_sprite(&mut self, x: u8, y: u8, sprite: &[u8]) -> BlitResult {
        let mut result = BlitResult::NotUnset;
        for (y_off, byte) in sprite.iter().cloned().enumerate() {
            if let BlitResult::Unset = self.blit_byte(x, (y+y_off as u8)%32, byte) {
                result = BlitResult::Unset;
            }
        }
        result
    }

    fn blit_byte(&mut self, x: u8, y: u8, byte: u8) -> BlitResult { 
        let mut result = BlitResult::NotUnset;
        for offset in 0..8 {
            let x = if x > 255 - offset {
                (x - (255 - offset)) % 64
            } else {
                (x + offset) % 64
            };
            let idx = x as usize + (y as usize * 64);
            let old_pix = self.pixels[idx];

            self.pixels[idx] ^= (byte & (0b10000000 >> offset)) >> (7-offset);

            if old_pix == 1 && self.pixels[idx] == 0 {
                result = BlitResult::Unset;
            }
        }
        result
    }
}

pub struct Chip8 {
    timer_init: u32,        // The value that timing counters start at
    delay_counter: u32,     // Counter that ticks dt when changed to 0
    sound_counter: u32,     // Counter that ticks st when changed to 0
    v: [u8; 16],            // Registers
    mem: [u8; 4096],        // 4kb mem, stack starts at 
    pc: u16,                // Program counter (instruction pointer)
    sp: u16,                // Stack pointer
    i: u16,                 // Address register
    dt: u8,                 // Delay timer register
    st: u8,                 // Audio timer register
    screen: Screen,         // Monochrome screen
    rng: ThreadRng,         // RNG
    key_states: [bool; 16], // State of each of the 16 keys
    awaiting: Option<u8>,   // A key to wait on before continuing
}

impl Chip8 {
    pub fn new(clock_rate: u32) -> Self {
        // Load font into memory. First 512 bytes are free 
        // for whatever purpose I want, so just dump them
        // at the start.
        let mut mem = [0; 4096];
        for (idx, character) in font::FONT.iter().enumerate() {
            let mem_start = idx*5;
            let mem_end = mem_start + 5;
            mem[mem_start..mem_end].copy_from_slice(character);
        }

        let timer_init = u32::max(clock_rate / 60, 1);
        Self {
            timer_init,
            delay_counter: timer_init,
            sound_counter: timer_init,
            v: [0; 16],
            mem,
            pc: MAIN_ADDR,
            sp: STACK_ADDR,
            i: 0,
            dt: 0,
            st: 0,
            screen: Screen::new(),
            rng: thread_rng(),
            key_states: [false; 16],
            awaiting: None,
        }
    }

    pub fn pixels(&self) -> &[u8] {
        &self.screen.pixels
    }

    pub fn load(&mut self, data: &[u8], addr: u16) {
        let write_start = addr as usize;
        let write_end = write_start + data.len();
        self.mem[write_start..write_end].copy_from_slice(data);
    }

    pub fn press_key(&mut self, key: u8) {
        // Reenable execution of program if awaited key is pressed
        if let Some(awaited_key) = self.awaiting {
            if awaited_key == key {
                self.awaiting = None;
            }
        }
        self.key_states[key as usize] = true;
    }

    pub fn release_key(&mut self, key: u8) {
        self.key_states[key as usize] = false;
    }

    pub fn clock(&mut self) {
        // Don't do anything if we're waiting on a key-press
        if self.awaiting.is_some() {
            return;
        }

        if self.dt != 0 {
            self.delay_counter -= 1;
            if self.delay_counter == 0 {
                self.delay_counter = self.timer_init;
            }
            self.dt -= 1;
            if self.dt == 0 {
                self.delay_counter = self.timer_init;
            }
        }

        if self.st != 0 {
            self.sound_counter -= 1;
            if self.sound_counter == 0 {
                self.sound_counter = self.timer_init;
            }
            self.st -= 1;
            if self.st == 0 {
                self.sound_counter = self.timer_init;
                // TODO: Disable buzzer.... also actually have a buzzer
            }
        }

        let op_code = self.next();
        match Op::from(op_code) {
            Op::CallRCA { .. } => eprintln!("Encountered unsupported call to RCA program"),
            Op::ClearDisplay => self.screen.clear(),
            Op::Ret => self.ret(),
            Op::Jp { addr } => self.jp(addr),
            Op::Call { addr } => self.call(addr),
            Op::Se { reg, byte } => self.se(reg, byte),
            Op::Sne { reg, byte } => self.sne(reg, byte),
            Op::SeReg { reg1, reg2 } => self.se_reg(reg1, reg2),
            Op::Ld { reg, byte } => self.ld(reg, byte),
            Op::Add { reg, byte } => self.add(reg, byte),
            Op::LdReg { reg1, reg2 } => self.ld_reg(reg1, reg2),
            Op::OrReg { reg1, reg2 } => self.or_reg(reg1, reg2),
            Op::AndReg { reg1, reg2 } => self.and_reg(reg1, reg2),
            Op::XorReg { reg1, reg2 } => self.xor_reg(reg1, reg2),
            Op::AddReg { reg1, reg2 } => self.add_reg(reg1, reg2),
            Op::SubReg { reg1, reg2 } => self.sub_reg(reg1, reg2),
            Op::ShrReg { reg1, reg2 } => self.shr_reg(reg1, reg2),
            Op::SubnReg { reg1, reg2 } => self.subn_reg(reg1, reg2),
            Op::ShlReg { reg1, reg2 } => self.shl_reg(reg1, reg2),
            Op::SneReg { reg1, reg2 } => self.sne_reg(reg1, reg2),
            Op::LdI { addr } => self.ldi(addr),
            Op::JpOff { addr } => self.jp_off(addr),
            Op::Rand { reg, mask } => self.rand(reg, mask),
            Op::Draw { reg1, reg2, nibble } => self.draw(reg1, reg2, nibble),
            Op::SKeyDown { reg } => self.skip_if_key_down(reg),
            Op::SKeyUp { reg } => self.skip_if_key_up(reg),
            Op::ReadDelay { reg } => self.read_delay(reg),
            Op::AwaitKey { reg } => self.await_key(reg),
            Op::WriteDelay { reg } => self.write_delay(reg),
            Op::WriteSound { reg } => self.write_sound(reg),
            Op::AddI { reg } => self.add_i(reg),
            Op::LdIFont { font_reg } => self.ldi_font(font_reg),
            Op::Bcd { reg } => self.bcd(reg),
            Op::RegDump { reg } => self.reg_dump(reg),
            Op::RegLoad { reg } => self.reg_load(reg),
            Op::Unknown => eprintln!("Unknown op code {:X} encountered", op_code),
        }
    }

    fn next(&mut self) -> u16 {
        let mut op = (self.mem[self.pc as usize] as u16) << 8;
        op |= self.mem[self.pc as usize + 1] as u16;
        self.pc += 2;
        op
    }

    fn draw(&mut self, r1: u8, r2: u8, nibble: u8) {
        let x = self.v[r1 as usize];
        let y = self.v[r2 as usize];

        let sprite_start = self.i as usize;
        let sprite_end = sprite_start + nibble as usize;
        let sprite = &self.mem[sprite_start..sprite_end];

        match self.screen.blit_sprite(x, y, sprite) {
            BlitResult::Unset => self.v[F] = 1,
            BlitResult::NotUnset => self.v[F] = 0,
        }
    }

    fn jp(&mut self, addr: u16) {
        self.pc = addr;
    }

    fn jp_off(&mut self, addr: u16) {
        self.pc = addr + self.v[0] as u16;
    }

    fn call(&mut self, addr: u16) {
        self.sp -= 2;
        self.mem[self.sp as usize] = (self.pc & 0x00FF) as u8;
        self.mem[self.sp as usize + 1] = ((self.pc & 0xFF00) >> 8) as u8;
        self.pc = addr;
    }

    fn ret(&mut self) {
        self.pc = self.mem[self.sp as usize] as u16;
        self.pc = (self.mem[self.sp as usize + 1] as u16) << 8;
        self.sp += 2;
    }

    fn ld(&mut self, r: u8, b: u8) {
        self.v[r as usize] = b;
    }
    
    fn ld_reg(&mut self, r1: u8, r2: u8) {
        self.v[r1 as usize] = self.v[r2 as usize];
    }

    fn ldi(&mut self, addr: u16) {
        self.i = addr;
    }

    fn add(&mut self, r: u8, b: u8) {
        let r_val = self.v[r as usize];
        self.v[r as usize] = if r_val <= 255 - b {
            r_val + b
        } else {
            (r_val - (255 - b)) - 1
        };
    }

    fn add_reg(&mut self, r1: u8, r2: u8) {
        let (r1_val, r2_val) = (self.v[r1 as usize], self.v[r2 as usize]);
        self.v[r1 as usize] = if r1_val <= 255 - r2_val {
            self.v[F] = 0;
            r1_val + r2_val
        } else {
            self.v[F] = 1;
            (r1_val - (255 - r2_val)) - 1
        };
    }

    fn add_i(&mut self, r: u8) {
        let r_val = self.v[r as usize] as u16;
        self.i += r_val;
        if self.i > 0x0FFF {
            self.i -= 0xF000;
        }
    }

    fn sub_reg(&mut self, r1: u8, r2: u8) {
        let (r1_val, r2_val) = (self.v[r1 as usize], self.v[r2 as usize]);
        self.v[r1 as usize] = if r1_val >= r2_val {
            self.v[F] = 1;
            r1_val - r2_val
        } else {
            self.v[F] = 0;
            (255 - (r2_val - r1_val)) + 1
        };
    }

    fn subn_reg(&mut self, r1: u8, r2: u8) {
        let (r1_val, r2_val) = (self.v[r1 as usize], self.v[r2 as usize]);
        self.v[r1 as usize] = if r2_val >= r1_val {
            r2_val - r1_val
        } else {
            (255 - (r1_val - r2_val)) + 1
        };
    }

    fn and_reg(&mut self, r1: u8, r2: u8) {
        self.v[r1 as usize] &= self.v[r2 as usize];
    }

    fn or_reg(&mut self, r1: u8, r2: u8) {
        self.v[r1 as usize] |= self.v[r2 as usize];
    }

    fn xor_reg(&mut self, r1: u8, r2: u8) {
        self.v[r1 as usize] ^= self.v[r2 as usize];
    }

    fn shr_reg(&mut self, r1: u8, r2: u8) {
        let r2_val = self.v[r2 as usize];
        self.v[F] = r2_val & 0b00000001;
        self.v[r1 as usize] = (r2_val & 0b11111110) >> 1;
    }

    fn shl_reg(&mut self, r1: u8, r2: u8) {
        let r2_val = self.v[r2 as usize];
        self.v[F] = r2_val & 0b10000000;
        self.v[r1 as usize] = (r2_val & 0b01111111) << 1;
    }

    fn se(&mut self, r: u8, b: u8) {
        if self.v[r as usize] == b {
            self.pc += 2;
        }
    }

    fn se_reg(&mut self, r1: u8, r2: u8) {
        if self.v[r1 as usize] == self.v[r2 as usize] {
            self.pc += 2;
        }
    }

    fn sne(&mut self, r: u8, b: u8) {
        if self.v[r as usize] != b {
            self.pc += 2;
        }
    }

    fn sne_reg(&mut self, r1: u8, r2: u8) {
        if self.v[r1 as usize] != self.v[r2 as usize] {
            self.pc += 2;
        }
    }

    fn reg_dump(&mut self, r: u8) {
        for reg in 0..=r as usize {
            self.mem[self.i as usize + reg] = self.v[reg];
        }
    }

    fn reg_load(&mut self, r: u8) {
        for reg in 0..=r as usize {
            self.v[reg] = self.mem[self.i as usize + reg];
        }
    }

    fn write_delay(&mut self, r: u8) {
        self.dt = self.v[r as usize];
    }

    fn read_delay(&mut self, r: u8) {
        self.v[r as usize] = self.dt;
    }

    fn write_sound(&mut self, r: u8) {
        self.st = self.v[r as usize];
    }

    fn rand(&mut self, r: u8, mask: u8) {
        self.v[r as usize] = self.rng.gen::<u8>() & mask;
    }
    
    fn ldi_font(&mut self, font_reg: u8) {
        let font_idx = self.v[font_reg as usize];
        self.i = font_idx as u16 * 5;
    }
    
    fn bcd(&mut self, r: u8) {
        let val = self.v[r as usize];
        let units = val % 10;
        let tens = val % 100;
        let hundreds = val / 100;
        self.mem[self.i as usize + 0] = hundreds;
        self.mem[self.i as usize + 1] = tens;
        self.mem[self.i as usize + 2] = units;
    }

    fn skip_if_key_down(&mut self, r: u8) {
        if self.key_states[self.v[r as usize] as usize] {
            self.pc += 2;
        }
    }

    fn skip_if_key_up(&mut self, r: u8) {
        if !self.key_states[self.v[r as usize] as usize] {
            self.pc += 2;
        }
    }

    fn await_key(&mut self, r: u8) {
        self.awaiting = Some(r);
    }
}
