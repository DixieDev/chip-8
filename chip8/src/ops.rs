#[allow(dead_code)]
#[derive(Debug)]
pub enum Op {
    CallRCA { addr: u16 },
    ClearDisplay,
    Ret,
    Jp { addr: u16 },
    Call { addr: u16 },
    Se { reg: u8, byte: u8 },
    Sne { reg: u8, byte: u8 },
    SeReg { reg1: u8, reg2: u8 },
    Ld { reg: u8, byte: u8 },
    Add { reg: u8, byte: u8 },
    LdReg { reg1: u8, reg2: u8 },
    OrReg { reg1: u8, reg2: u8 },
    AndReg { reg1: u8, reg2: u8 },
    XorReg { reg1: u8, reg2: u8 },
    AddReg { reg1: u8, reg2: u8 },
    SubReg { reg1: u8, reg2: u8 },
    ShrReg { reg1: u8, reg2: u8 },
    SubnReg { reg1: u8, reg2: u8 },
    ShlReg { reg1: u8, reg2: u8 },
    SneReg { reg1: u8, reg2: u8 },
    LdI { addr: u16 },
    JpOff { addr: u16 },
    Rand { reg: u8, mask: u8 },
    Draw { reg1: u8, reg2: u8, nibble: u8 },
    SKeyDown { reg: u8 },
    SKeyUp { reg: u8 },
    ReadDelay { reg: u8 },
    AwaitKey { reg: u8 },
    WriteDelay { reg: u8 },
    WriteSound { reg: u8 },
    AddI { reg: u8 },
    LdIFont { font_reg: u8 },
    Bcd { reg: u8 },
    RegDump { reg: u8 },
    RegLoad { reg: u8 },
    Unknown,
}

impl From<u16> for Op {
    fn from(opcode: u16) -> Self {
        // Extract useful values
        let header = ((opcode & 0xF000) >> 12) as u8;
        let reg1 = ((opcode & 0x0F00) >> 8) as u8;
        let reg2 = ((opcode & 0x00F0) >> 4) as u8;
        let byte = (opcode & 0x00FF) as u8;
        let addr = opcode & 0x0FFF;
        let footer = (opcode & 0x000F) as u8;

        // Convert opcode to Op variant
        match header {
            0x0 => match byte {
                0xE0 => Self::ClearDisplay,
                0xEE => Self::Ret,
                _ => Self::CallRCA { addr },
            },
            0x1 => Self::Jp { addr },
            0x2 => Self::Call { addr },
            0x3 => Self::Se { reg: reg1, byte },
            0x4 => Self::Sne { reg: reg1, byte },
            0x5 => Self::SeReg { reg1, reg2 },
            0x6 => Self::Ld { reg: reg1, byte },
            0x7 => Self::Add{ reg: reg1, byte },
            0x8 => match footer {
                0x0 => Self::LdReg { reg1, reg2 },
                0x1 => Self::OrReg { reg1, reg2 },
                0x2 => Self::AndReg { reg1, reg2 },
                0x3 => Self::XorReg { reg1, reg2 },
                0x4 => Self::AddReg { reg1, reg2 },
                0x5 => Self::SubReg { reg1, reg2 },
                0x6 => Self::ShrReg { reg1, reg2 },
                0x7 => Self::SubnReg { reg1, reg2 },
                0xE => Self::ShlReg { reg1, reg2 },
                _ => Self::Unknown,
            },
            0x9 => match footer {
                0x0 => Self::SneReg { reg1, reg2 },
                _ => Self::Unknown,
            },
            0xA => Self::LdI { addr },
            0xB => Self::JpOff { addr },
            0xC => Self::Rand { reg: reg1, mask: byte },
            0xD => Self::Draw { reg1, reg2, nibble: footer },
            0xE => match byte {
                0x9E => Self::SKeyDown { reg: reg1 },
                0xA1 => Self::SKeyUp { reg: reg1 },
                _ => Self::Unknown,
            },
            0xF => match byte {
                0x07 => Self::ReadDelay { reg: reg1 },
                0x0A => Self::AwaitKey { reg: reg1 },
                0x15 => Self::WriteDelay { reg: reg1 },
                0x18 => Self::WriteSound { reg: reg1 },
                0x1E => Self::AddI { reg: reg1 },
                0x29 => Self::LdIFont { font_reg: reg1 },
                0x33 => Self::Bcd { reg: reg1 },
                0x55 => Self::RegDump { reg: reg1 },
                0x65 => Self::RegLoad { reg: reg1 },
                _ => Self::Unknown,
            },
            _ => unreachable!(),
        }
    }
}

