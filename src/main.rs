use chip8::Chip8;
use std::{ time::{Duration, Instant}, io::Read, fs::File };
use sdl2::{ pixels::{Color, PixelFormatEnum}, render::TextureAccess, event::Event, keyboard::Keycode };

fn main() {
    // Determine rom file by args
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        eprintln!("Incorrect number of args. Please specify a rom file to load");
        return;
    }

    // Load rom to byte array
    let rom: Vec<u8> = File::open(&args[1])
        .unwrap()
        .bytes()
        .filter_map(|r| r.ok())
        .collect();

    // Chip-8 setup
    let clock_rate = 500;
    let mut chip8 = Chip8::new(clock_rate);
    chip8.load(&rom, chip8::MAIN_ADDR);

    // Timing setup
    let frame_duration = Duration::from_secs_f64(0.016);
    let mut now = Instant::now();
    let mut timer = 0.0;

    // SDL setup
    let sdl = sdl2::init().unwrap();
    let video = sdl.video().unwrap();

    let window = video.window("Chip-8", 640, 320)
        .position_centered()
        .build()
        .unwrap();

    let mut canvas = window.into_canvas().build().unwrap();
    canvas.set_draw_color(Color::RGB(0, 0, 0));
    canvas.clear();
    canvas.present();

    // SDL render target setup
    let texture_creator = canvas.texture_creator();
    let mut texture = texture_creator
        .create_texture(
            Some(PixelFormatEnum::ABGR8888),
            TextureAccess::Streaming,
            64,
            32,
        )
        .unwrap();
    let channels = 4;

    // Main loop
    let mut event_pump = sdl.event_pump().unwrap();
    let mut active = true;
    while active {
        // Sort out time nonsense
        let frame_start = Instant::now();
        let dt = (frame_start - now).as_secs_f64();
        now = frame_start;

        // Handle events
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit {..} => active = false,
                Event::KeyDown { keycode: Some(Keycode::Num1), .. } => chip8.press_key(0x1),
                Event::KeyDown { keycode: Some(Keycode::Num2), .. } => chip8.press_key(0x2),
                Event::KeyDown { keycode: Some(Keycode::Num3), .. } => chip8.press_key(0x3),
                Event::KeyDown { keycode: Some(Keycode::Num4), .. } => chip8.press_key(0xC),
                Event::KeyDown { keycode: Some(Keycode::Q), .. } => chip8.press_key(0x4),
                Event::KeyDown { keycode: Some(Keycode::W), .. } => chip8.press_key(0x5),
                Event::KeyDown { keycode: Some(Keycode::E), .. } => chip8.press_key(0x6),
                Event::KeyDown { keycode: Some(Keycode::R), .. } => chip8.press_key(0xD),
                Event::KeyDown { keycode: Some(Keycode::A), .. } => chip8.press_key(0x7),
                Event::KeyDown { keycode: Some(Keycode::S), .. } => chip8.press_key(0x8),
                Event::KeyDown { keycode: Some(Keycode::D), .. } => chip8.press_key(0x9),
                Event::KeyDown { keycode: Some(Keycode::F), .. } => chip8.press_key(0xE),
                Event::KeyDown { keycode: Some(Keycode::Z), .. } => chip8.press_key(0xA),
                Event::KeyDown { keycode: Some(Keycode::X), .. } => chip8.press_key(0x0),
                Event::KeyDown { keycode: Some(Keycode::C), .. } => chip8.press_key(0xB),
                Event::KeyDown { keycode: Some(Keycode::V), .. } => chip8.press_key(0xF),
                Event::KeyUp { keycode: Some(Keycode::Num1), .. } => chip8.release_key(0x1),
                Event::KeyUp { keycode: Some(Keycode::Num2), .. } => chip8.release_key(0x2),
                Event::KeyUp { keycode: Some(Keycode::Num3), .. } => chip8.release_key(0x3),
                Event::KeyUp { keycode: Some(Keycode::Num4), .. } => chip8.release_key(0xC),
                Event::KeyUp { keycode: Some(Keycode::Q), .. } => chip8.release_key(0x4),
                Event::KeyUp { keycode: Some(Keycode::W), .. } => chip8.release_key(0x5),
                Event::KeyUp { keycode: Some(Keycode::E), .. } => chip8.release_key(0x6),
                Event::KeyUp { keycode: Some(Keycode::R), .. } => chip8.release_key(0xD),
                Event::KeyUp { keycode: Some(Keycode::A), .. } => chip8.release_key(0x7),
                Event::KeyUp { keycode: Some(Keycode::S), .. } => chip8.release_key(0x8),
                Event::KeyUp { keycode: Some(Keycode::D), .. } => chip8.release_key(0x9),
                Event::KeyUp { keycode: Some(Keycode::F), .. } => chip8.release_key(0xE),
                Event::KeyUp { keycode: Some(Keycode::Z), .. } => chip8.release_key(0xA),
                Event::KeyUp { keycode: Some(Keycode::X), .. } => chip8.release_key(0x0),
                Event::KeyUp { keycode: Some(Keycode::C), .. } => chip8.release_key(0xB),
                Event::KeyUp { keycode: Some(Keycode::V), .. } => chip8.release_key(0xF),
                _ => (),
            }
        }

        // Clock Chip-8
        timer += dt * clock_rate as f64;
        while timer as u32 > 0 {
            chip8.clock();
            timer -= 1.0;
        }

        // Draw Chip-8 screen to pixel array
        texture.with_lock(None, |tex_pixels: &mut [u8], _pitch: usize| {
            for (i, chip_pixel) in chip8.pixels().iter().cloned().enumerate() {
                let channel_pixel = if chip_pixel == 0 { 0 } else { 255 };
                tex_pixels[i*channels+0] = channel_pixel;
                tex_pixels[i*channels+1] = channel_pixel;
                tex_pixels[i*channels+2] = channel_pixel;
                tex_pixels[i*channels+3] = channel_pixel;
            }
        }).unwrap();

        // Draw texture to window
        canvas.clear();
        canvas.copy(&texture, None, None).unwrap();
        canvas.present();

        // Stall until min frame duration is reached
        while (Instant::now()-now) < frame_duration { }
    }
}
